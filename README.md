# Mosaic Pools by Mosaicist

Mosaicist makes any swimming pool or water facade full of character and beauty, from modern to old renaissance designs or motifs. We specialize in swimming pools, spas, showers, fountains, and many other water facades. Must of our selections are custom and personalized. Our products install flush and have a smooth flat finish. The Outcome can change an entire appearance to swimming pools and water facades.		
Web: https://mosaicist.com/